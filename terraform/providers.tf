terraform {
  required_version = ">= 1.1"
}

provider "aws" {
  region     = "us-west-1"
}

data "aws_availability_zones" "available" {}

provider "http" {}
